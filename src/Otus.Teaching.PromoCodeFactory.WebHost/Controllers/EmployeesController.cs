﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(r => new RoleItemResponse()
                {
                    Id = r.Id,
                    Name = r.Name,
                    Description = r.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника.
        /// </summary>
        /// <param name="requestModel">Модель запроса</param>
        /// <returns>Id сотрудника</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> PostEmployeeAsync(EmployeeCreateRequest request)
        {
            var roles = new List<Role>();
            foreach (var r in request.Roles)
            {
                var role = await _rolesRepository.GetByIdAsync(r);
                if (role == default(Role))
                {
                    return NotFound("User role does not exist");
                }
                roles.Add(role);
            }

            var id = await _employeeRepository.AddAsync(new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Roles = roles
            });
            return Ok(id);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == default(Employee))
            {
                return NotFound();
            }

            await _employeeRepository.DeleteAsync(id);
            return Ok();
        }

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <param name="request">Модель запроса</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployeeAsync(EmployeeUpdateRequest request)
        {
            var employee = await _employeeRepository.GetByIdAsync(request.Id);
            if (employee == default(Employee))
            {
                return NotFound();
            }
            employee.FirstName= request.FirstName;
            employee.LastName= request.LastName;
            employee.Email= request.Email;
            employee.Roles = new List<Role>();

            foreach (var r in request.Roles)
            {
                var role = await _rolesRepository.GetByIdAsync(r);
                if (role == default(Role))
                {
                    return NotFound("User role does not exist");
                }

                employee.Roles.Add(role);
            }
            
            await _employeeRepository.UpdateAsync(employee);
            return Ok();
        }
    }
}