﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}